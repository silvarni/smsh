#include "jobs.h"

int jobs_add(struct jobs* jobs, pid_t pid)
{
    if (jobs->njobs + 1 == JOBS_MAX_SIZE)
    {
        print_error("Can't add more background job.\n");
        return 0;
    }
    jobs->pids[jobs->njobs++] = pid;
    //printf("Saving pid %d \n", pid); //debug
    return 0;
}

void jobs_kill(struct jobs* jobs, int sig)
{
    for (size_t i = 0; i < jobs->njobs; i++)
        if (jobs->pids[i] && kill(jobs->pids[i], sig))
            perror("kill");
}

void jobs_wait(struct jobs* jobs)
{
    for (size_t i = 0; i < jobs->njobs; i++)
    {
        if (jobs->pids[i])
        {
            int status;
            if (waitpid(jobs->pids[i], &status, 0) == -1)
                perror("wait");
            else
                print_status(status);
        }
    }
    jobs->njobs = 0;
}

