#include "intern_command.h"

int intern_command_manage(const struct cmd* command, struct jobs* bg_jobs)
{
    assert(command);
    const char* command_name = command->args[0ul];

    if ( strcmp(command_name, "exit") == 0 )
    {
        intern_command_exit(bg_jobs);
        return 0;
    }
    else if ( strcmp(command_name, "cd") == 0 )
    {
        switch( command->nargs )
        {
            case 1ul:
                intern_command_cd(getenv("HOME"));
                break;

            case 2ul:
                intern_command_cd(command->args[1]);
                break;

            default:
                intern_command_cd(NULL); // Print usage
                break;
        }

        return 0;
    }

    return -1;
}

void intern_command_exit(struct jobs* bg_jobs)
{
    jobs_kill(bg_jobs, SIGTERM);
    jobs_wait(bg_jobs);
    exit(EXIT_SUCCESS);
}

void intern_command_cd(const char* dir)
{
    if ( dir == NULL )
    {
        // Usage
        print_error("cd: <dir>\n\t- If $HOME is set, dir can be ignored and will be $HOME \n.");
    }
    else
    {
        wordexp_t* we = intern_command_extend_regex(dir);
        char** extended = we->we_wordv;

        if ( chdir(extended[0]) )
            perror("cd");

        wordfree(we);
    }
    //fprintf(stderr, "PWD is %s (asked %s)\n", getcwd(pwd, PATH_MAX), dir); // debug
}

const char* intern_command_get_pwd(void)
{
    getcwd(pwd, PATH_MAX * sizeof(char));
    //fprintf(stderr, "Changed pwd into {%s}\n", pwd); //debug
    return pwd;

}


wordexp_t* intern_command_extend_regex(const char* regex)
{
    wordexp_t* we = malloc(sizeof(wordexp_t));

    int r = wordexp(regex, we, 0);
    switch(r)
    {
        case WRDE_BADCHAR :
            {print_error("cd: Illegal character in argument.\n"); return we;}
            break;

        case WRDE_NOSPACE :
            {print_error("cd: Out of memory.\n");return we;}
            break;

        case WRDE_SYNTAX :
            {print_error("cd: wrong syntax.\n");return we;}
            break;
    }

    return we;
}
