#include "output.h"

#include "intern_command.h"

#include <sys/wait.h>

void print_error(const char* msg)
{
  fprintf(stderr, "%s%s%s",RED, msg, CLEAR);
}

void print_prompt(void)
{
  printf("%s[%ssmsh%s] %s %s %s \n$ ", PINK, GREEN, PINK, BLUE, intern_command_get_pwd(), CLEAR);
}

void print_status(int status)
{
    // 2.2 gestion du code retour de la commande
    if (WIFEXITED(status) == 0)
        printf("The program does not end correctly. Return code : %d.\n", WEXITSTATUS(status));
    // 2.2 gestion du code signal reçu
    if (WIFSIGNALED(status))
        printf("The program received the signal %d.\n", WTERMSIG(status));
}

