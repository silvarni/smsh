#include "smsh.h"

static struct jobs bg_jobs;

// Handler of SIGTERM signal
void handSIGCHLD(int sig)
{
    int status;
    for (size_t i = 0; i < bg_jobs.njobs; i++)
    {
        status = 0;
        if (bg_jobs.pids[i] && waitpid(bg_jobs.pids[i], &status, WNOHANG) == bg_jobs.pids[i])
        {
            bg_jobs.pids[i] = 0;
            print_status(status);
        }
    }
}

int main()
{
    struct line li;
    char buf[BUFLEN];
    bg_jobs.njobs = 0;

    // Ctrl + C : utilisation du gestionaire SIG_IGN sur le shell
    struct sigaction sa, saDefSIGINT, saDefSIGCHLD;
    sa.sa_handler = SIG_IGN;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;

    // Application du sigaction avec récupération de l'ancien gestionnaire de signal par défaut
    if (sigaction(SIGINT, &sa, &saDefSIGINT) == -1)
        perror("sigaction");

    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    sa.sa_handler = handSIGCHLD;
    if (sigaction(SIGCHLD, &sa, &saDefSIGCHLD) == -1)
        perror("sigaction");

    while(1) // far better than for(;;)
    {
        print_prompt();
        fgets(buf, BUFLEN, stdin);
        line_init(&li);

        int err = line_parse(&li, buf, strlen(buf));
        if (err) {
            return 0;
        }

        // création de la pile d'exécution des commandes
        struct jobs fg_jobs;
        fg_jobs.njobs = 0;

        // Exercice 7 : pipes creations
        int* pipefd = calloc(li.ncmds * 2, sizeof(int));
        memset(pipefd, -1, li.ncmds * sizeof(int));

        // Exercice 2 : cas simple une seule commande avec ou sans arguments (2.1 & 2.3)
        for (size_t i = 0; i < li.ncmds; i++)
        {
            // > Exercice 3 : si commande interne, traité dans intern_command.c
            if ( intern_command_manage( &li.cmds[i], &bg_jobs ) == -1) // > -1 if extern
            {
                // ouverture du pipe si i n'est pas la dernière commande
                if (i + 1 < li.ncmds)
                    if (pipe(pipefd + 2 * i) == -1)
                        perror("pipe");

                // Exercice 4 : redirections
                // Ouverture des fichiers
                int fdin = -1;
                int fdout = -1;
                if (li.redirect_input)
                {
                    fdin = open(li.file_input, O_RDONLY);
                    if ( fdin == -1 )
                        perror(li.file_input);
                }
                if (li.redirect_output)
                {
                    fdout = open(li.file_output, O_WRONLY | O_CREAT, 0644);
                    if (fdout == -1)
                        perror(li.file_output);
                }

                pid_t child_pid = fork();
                if (child_pid == -1)
                    perror("fork");
                else if (child_pid == 0)
                {
                    // redirection de la sortie standard dans le pipe i
                    if (i + 1 < li.ncmds)
                    {
                        dup2(pipefd[2 * i + 1], 1);
                        if (close(pipefd[2 * i + 1]) == -1)
                            perror("close");
                        if (close(pipefd[2 * i]) == -1)
                            perror("close");
                    }
                    // redirection de l'entrée standard depuis le pipe i - 1
                    if (i > 0)
                    {
                        dup2(pipefd[2 * (i - 1)], 0);
                        if (close(pipefd[2 * (i - 1)]) == -1)
                            perror("close");
                    }

                    // redirections de l'entrée et de la sortie standard
                    if (fdin != -1)
                        dup2(fdin, 0);
                    if (fdout != -1)
                        dup2(fdout, 1);

                    // utilisation du gestionnaire de signal par défaut pour SIGINT
                    if (sigaction(SIGINT, &saDefSIGINT, NULL) == -1)
                        perror("sigaction");
                    if (sigaction(SIGCHLD, &saDefSIGCHLD, NULL) == -1)
                        perror("sigaction");

                    // Gestion des arguments (BONUS)
                    size_t nargs = li.cmds[i].nargs; // nargs of cmd befor extend
                    wordexp_t* regexs = calloc(nargs, sizeof(wordexp_t)); // array of regex
                    size_t final_narg = 0ul; // number of args after extends

                    for (size_t j = 0ul ; j < nargs ; ++j)
                    {
                        regexs[j] = *intern_command_extend_regex( li.cmds[i].args[j] );
                        final_narg += regexs[j].we_wordc;
                        //fprintf(stderr, "Extending arg %s into %zu new args\n", li.cmds[i].args[j], regexs[j].we_wordc); // debug
                    }

                    char** final_args = calloc(final_narg + 1, sizeof(char*));
                    size_t index = 0ul;
                    for (size_t j = 0ul ; j < nargs ; ++j )
                    {
                        for ( size_t k = 0ul ; k < regexs[j].we_wordc ; ++k )
                        {
                            assert(index < final_narg);
                            final_args[index] = regexs[j].we_wordv[k] ;
                            //fprintf(stderr, "\tArg n°%zu is %s\n", index, final_args[index]); // debug
                            index++;
                        }
                    }
                    final_args[final_narg] = NULL; // Not necessary with calloc

                    /// BIG MEMORY LEAK

                    // exécution du processus avec envoie des arguments (ou pas)
                    execvp(li.cmds[i].args[0], final_args);
                    perror("Erreur lors de l'exécution");
                }
                else
                {
                    // fermeture de la sortie du pipe i - 1
                    if (i + 1 < li.ncmds)
                        close(pipefd[2 * i + 1]);
                    if (i > 0)
                        close(pipefd[2 * (i - 1)]);

                    // Exercice 6 : processus en arrière plan ou dans la pile
                    if ( li.background )
                        jobs_add(&bg_jobs, child_pid);
                    else
                        jobs_add(&fg_jobs, child_pid);
                }

                // fermeture des fichiers utilisés pour la redirection
                if (fdin != -1)
                    close(fdin);
                if (fdout != -1)
                    close(fdout);
            }
        }
        /*for (size_t i = 0; i < fg_jobs.njobs; i++) // debug*/
	/*fprintf(stderr, "job %d\n", fg_jobs.pids[i]);*/
        jobs_wait(&fg_jobs);
        free(pipefd);

        line_reset(&li);
    }
}


