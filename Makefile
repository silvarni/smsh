SRC_DIR = src
HEADER_DIR = include
OBJ_DIR = obj
TEST_DIR = test
EXEC_DIR = bin

CXX = clang
CXXFLAGS = -Wall -Werror -g -I"include"  -I"${HEADER_DIR}" --std=gnu99 # -DNO_COLOR # with no color
LFLAGS =

TARGET = smsh.elf
SRC = ${wildcard ${SRC_DIR}/*.c}
OBJ = ${SRC:${SRC_DIR}/%.c=${OBJ_DIR}/%.o}

green = \033[32m
red   = \033[31m
pink  = \033[35m
blink = \033[5m
reset = \033[0m
bold  = \033[1m

all: ${EXEC_DIR}/${TARGET} test
	@echo -e "${blink} ${pink} Make ended ${bold} successfully !${reset}"

test : cmdline_test

run : ${EXEC_DIR}/${TARGET}
	${EXEC_DIR}/${TARGET}

mkdir:
	@if [ ! -d bin ]; then mkdir bin; fi
	@if [ ! -d obj ]; then mkdir bin; fi

cmdline_test: ${OBJ_DIR}/cmdline_test.o ${OBJ_DIR}/cmdline.o
	${CXX} ${CXXFLAGS} ${LFLAGS} $^ -o ${EXEC_DIR}/test.elf
	@echo -e "${green} Tests linking completed ${reset}"

${EXEC_DIR}/${TARGET} : mkdir ${OBJ}
	${CXX} ${CXXFLAGS} ${LFLAGS} ${OBJ} -o $@
	@echo -e "${green} SMSH linking completed ${reset}"

# Special files with no header
${OBJ_DIR}/cmdline_test.o : ${TEST_DIR}/cmdline_test.c ${SRC_DIR}/smsh.c 
	${CXX} ${CXXFLAGS} $< -c -o $@
	@echo -e "\t${green} tests ${bold} compiled ${reset}"
##

# Generic rule
${OBJ_DIR}/%.o: ${SRC_DIR}/%.c ${HEADER_DIR}/%.h
	${CXX} ${CXXFLAGS} $< -c -o $@
	@echo -e "\t${green} $< ${bold} compiled ${reset}"	

# Cleaning
clean :
	rm -rf ${OBJ_DIR}/*.o
	@echo -e "\t${green} Object files has successfully been ${bold} removed ${reset}"

mrproper: clean
	rm -rf ${EXEC_DIR}/*.elf
	@echo -e "\t${green} Executable files has successfully been ${bold} removed${reset}"
