/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/



#ifndef OUTPUT_HPP
#define OUTPUT_HPP

// > Uncomment for no color
// #define NO_COLOR

#if !( defined NO_COLOR || defined COLOR_DEF)
#define COLOR_DEF
#define RED "\033[031;1m"
#define CLEAR "\033[0m"
#define GREEN "\033[32m"
#define PINK "\033[35m"
#define BLUE "\033[36m"

#else
#define COLOR_DEF
#define RED ""
#define CLEAR ""
#define GREEN ""
#define PINK ""
#define BLUE ""

#endif



#include <stdio.h>

/**
 * Print a message on stderr
 * @param msg The error msg
 */
void print_error(const char* msg);

/**
 * Print the prompt.
 */
void print_prompt(void);

/**
 * Print a status
 * @param status The status to print
 */
void print_status(int status);
#endif
