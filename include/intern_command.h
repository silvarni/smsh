/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* intern_command */
#ifndef INTERN_COMMAND_HPP
#define INTERN_COMMAND_HPP

#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <wordexp.h>

#include "output.h"
#include "cmdline.h"
#include "jobs.h"
#include "smsh.h"

#define PATH_MAX 256
#include <wordexp.h>
struct cmd;


static char pwd[PATH_MAX];

/**
 * If it is an intern command, it executes it.
 * @param command The  command
 * @return -1 if not, 0 otherwise
 */
int intern_command_manage(const struct cmd* command, struct jobs* bg_jobs);

/**
 * Exit the sm-shell.
 */
void intern_command_exit(struct jobs* bg_jobs);

/**
 * Change current directory
 * @param dir The dir wished
 */
void intern_command_cd(const char* dir);

/**
 * Set pwd global variable to current directory
 */
const char* intern_command_get_pwd(void);

/**
 * Extends a regex shell-like (dynamicly allocated, think to free)
 * @param regex The regex
 * @return The wordextend struct
 */
wordexp_t* intern_command_extend_regex(const char* regex);

#endif
