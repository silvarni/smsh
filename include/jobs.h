/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef JOBS_H
#define JOBS_H

#include "output.h"

#include <signal.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

#define JOBS_MAX_SIZE 64

struct cmd;

struct jobs
{
  pid_t pids[JOBS_MAX_SIZE];
  size_t njobs;
};

/**
 * Keep in mind the pid of a child
 * @param pid The child pid
 * @return 1 if to many background job, 0 otherwise
 */
int jobs_add(struct jobs* jobs, pid_t pid);

/**
 *
 */
void jobs_kill(struct jobs* jobs, int sig);

void jobs_wait(struct jobs* jobs);

#endif

