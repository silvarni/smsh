# SMSH (Silvert-Mekarni SHell )
Projet créé par Ervan Silvert et Romain Mekarni le 25 Avril 2016

Statut : *Terminé*
- - -

## Remarque
Pour une meilleure lecture, visualisez ce document avec un lecteur de Markdown, ou sur le lien suivant [https://gitlab.com/silvarni/smsh](https://gitlab.com/silvarni/smsh).
- - -

## Description
**SMSH** est un shell basique destiné à appréhender les notions de bases de la gestion de processus.

**SMSH** supporte l'execution de toutes commandes supportées par **`exec(3)`**

**SMSH** sait étendre les expressions comportant un joker / wildcard. Il utilise **`wordexp(3)`**.

**SMSH** utilise un affichage couleur pour le confort de l'utilisateur. Toutefois, si celles-ci sont mal supportées ou inconfortables, il est possible de les enlever en définissant la variable `NO_COLOR` dans le flag du `Makefile` ou directement dans un fichier source.
- - -

## Installation
`$ cd installation/dir`

`$ git clone https://gitlab.com/silvarni/smsh.git`

`$ make # génére les test et l'exécutable` 

`$ ./bin/smsh.elf # execution`
- - -

## Fonctionnement général
### Parsing de la ligne de commande
Le parsing de la ligne de commande été donnée dans le sujet, `cmd_line.h` possède une structure `line` et `cmd` qui donnent toutes les informations nécessaires. Leur interprétation était en revanche notre devoir.

* Commandes internes
Les commandes internes sont :
* `exit`:    Permet de quitter proprement le terminal et libérer la mémoire.
* `cd [path]`:    Permet de changer de répertoire courant. Si aucun argument n'est donné, la valeur de `$HOME` est utilisée. Le chemin est interprété par `wordexp(3)` afin de substituer les chemins `"~/./../"`.

### Gestion des processus
Nous avons regroupé dans `jobs.h` l'ensemble des méthodes permettant de gérer les processus. Dans `smsh.c` deux tableaux de `jobs` sont initialisés, permettant la gestion séparée des taches de fond ou de front ( respectivement `bg_jobs` et `fg_jobs`).

Lors d'une commande en premier plan, tous les processus sont stockés dans ce dernier tableau puis attendus.

Lors d'une commande en arrière plan, le processus est ajouté au tableau de fond qui est traité à la sortie du terminal (ie. après un `exit` par exemple), c'est-à-dire tués (`kill(3)`) et attendus (`waitpid(3)`).

### Gestion des redirections
Deux `file descriptors` (fd) sont prêt à l'emploi pour chaque ligne de commande.

Si une redirection de l'entrée est demandée, `fdin` est ouvert avec `open(3)` puis `dup2(3)` avec l'entrée standart.

Si une redirection de la sortie est demandée, le même procédé est utilisé avec `fdout`.

### Gestion des communications inter-processus (pipe)
Un tableau de pipe est alloué dynamiquement avant l'exécution des commandes. Pour `n` commandes, `2n` fd sont alloués et initialisé à `-1` (erreur). Puis chaque processus se remplissent les cases dans l'ordre `stdin` puis `stdout`.

## Bilan
**SMSH** supporte toutes les commandes de `exec(3)`, comprend les wildcards / jokers, comprend les pipes et taches de fond.

**SMSH** ne supporte pas la complétion automatique ou le comportement conditionnel (`&&` ou `||`) bien que ce dernier point soit très facilement implémentable avec notre structure. 

## Tests supportés
L'exécutable offert de test passe entièrement sans échec, chacun des points cités plus haut a été testé de nombreuses fois dans des circonstances très variées mais sous un unique système d'exploitation `Archlinux 64bits`.

## Extensions apportées
L'extension des arguments par `wordexp(3)` est le seul bonus que nous avons apporté au projet avec le comportement par défaut de `cd` si la variable d'environnement `$HOME` est définie. 

